

<?php


require "twitteroauth/autoload.php";

// loads TwitterOUTH

use Abraham\TwitterOAuth\TwitterOAuth;

// Given a twitter screen name, returns the html for a widget containing their last 20 tweets

function twitter($user_name)
{
	$out = "";
	$out.= '<link rel="stylesheet" href="styles.css">';
	$out.= '<script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>'; // for twitter web intents
	$connection = new TwitterOAuth("IdBOUDzhXNLjr9wem52bazCcN", "9nUNgbf4ytUMyH6YRiVAlDrayk3D6QYkg1MEmaFiZMoyaCjNfC", "942630331-qFCESSG9jbqAeBK9ggll2T0SOImb0PSXP98xMHHj", "Lhkrejeyc6Qhi6eqzdUlPWfHUHSQsuWgaRMAqHb5fktAh");
	$string = $connection->get('statuses/user_timeline', array(
		'screen_name' => $user_name,
		'count' => 20
	));
	if ($connection->getLastHttpCode() == 200) {
		$out.= "<div class='twitterWidget'>";
		$count = 0;
		foreach($string as $items) {
			$out.= render_tweet($items, $count);
			$count++;
		}

		$out.= "</div>";
	}
	else {
		$out.= "<h3>Sorry, there was a problem.</h3><p>Twitter returned the following error message:</p><p><em>" . htmlentities($string->errors[0]->message) . "</em></p>";
		exit();
	}

	return $out;
}

function render_tweet($items, $count)
{
	$out = "";
	if ($count == 0) {
		$out.= "<div class='tweetHeader'>";
		$out.= "<a target='_blank' href='https://www.twitter.com/intent/user?screen_name=" . htmlentities($items->user->screen_name) . "' class='proPic'>" . "<img class='proPic' src='" . htmlentities($items->user->profile_image_url_https) . "'></img>" . "</a>";
		$out.= "<a target='_blank' href='https://www.twitter.com/intent/user?screen_name=" . htmlentities($items->user->screen_name) . "' class='name'>" . htmlentities($items->user->name) . "</a>";
		$out.= "<a target='_blank' href='https://www.twitter.com/intent/user?screen_name=" . htmlentities($items->user->scren_name) . "' class='screen_name'>" . "@" . htmlentities($items->user->screen_name) . "</a>";
		$out.= "</div>";
	}

	$out.= "<div class='tweet'><div class='tweetText'>" . parse_tweet(htmlentities($items->text)) . "</div>";
	if (!empty(htmlentities($items->entities->media[0]->media_url))) {
		$media_url = htmlentities($items->entities->media[0]->media_url);
		$out.= "<a target='_blank' href='" . htmlentities($items->entities->media[0]->url) . "'><img class='tweetImage' alt='' src='" . $media_url . "'></img></a>";
	}

	$out.= '<div class=tweetTools"><a href="https://twitter.com/intent/retweet?tweet_id=' . htmlentities($items->id) . '"><img class="twitterButton" src="https://g.twimg.com/dev/documentation/image/retweet-icon-16.png" alt="Retweet"></img></a>';
	$out.= '<a href="https://twitter.com/intent/like?tweet_id=' . htmlentities($items->id) . '"><img class="twitterButton" src="https://g.twimg.com/dev/documentation/image/like-icon-16.png" alt="Like"></img></a>';
	$out.= '<a href="https://twitter.com/intent/tweet?in_reply_to=' . htmlentities($items->id) . '"><img class="twitterButton" src="https://g.twimg.com/dev/documentation/image/reply-icon-16.png" alt="Reply"></img></a>';
	$out.= '</div>';
	$out.= '</div>';
	return $out;
}

function parse_tweet($text)
{
	$text = utf8_decode($text);

	// links

	$text = preg_replace('@(https?://([-\w\.]+)+(/([\w/_\.]*(\?\S+)?(#\S+)?)?)?)@', '<a target="_blank" href="$1">$1</a>', $text);

	// users

	$text = preg_replace('/@(\w+)/', '<a target="_blank" href="http://twitter.com/intent/user?screen_name=$1">@$1</a>', $text);

	// hashtags

	$text = preg_replace('/\s+#(\w+)/', ' <a target="_blank" href="http://twitter.com/search?q=%23$1">#$1</a>', $text);
	return $text;
}


?>

<?php echo  twitter("michigandaily"); ?>